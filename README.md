# Implement densely connected convolutional network with TensorFlow framework


## Read and split data: using read_data.py file
- SRC_PATH: directory contains raw images
- DST_PATH: directory to save *.npy file


## Training model: using train.py file
- input_size: Size of the input image. If it smaller than original image, we use random crop in data augmentation phase
- num_class: Number of classes of our task
- init_lnr: The started learning rate when we train our model
- depth: It should be: total_blocks . x + total_blocks + 1
- bc_mode: Using bc mode or not
- reduction: Compression parameter, it should equal to 1.0 if bc_mode = False
- lnr_update_center: Learning rate for updating center if you wanna use center loss
- weight_center_loss: Weight of center loss 
- reduce_lnr: When you reduce the model's learning rate?